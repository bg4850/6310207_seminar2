﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_631020207_Seminar2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body style="height: 1100px">
    <form id="form1" runat="server">
    <div>
    
        <asp:Label ID="L_Hrm" runat="server" Font-Bold="True" Font-Size="XX-Large" Text="HRM"></asp:Label>
    
    </div>
        <hr />
        <div>
            <asp:Label ID="L_DepartmentGroup" runat="server" Font-Bold="True" Text="Organizacijska enota:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="DDL_DepartmentGroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_DeparmentGroup_SelectedIndexChanged">
            </asp:DropDownList>
            <br />
            <asp:Label ID="L_Department" runat="server" Font-Bold="True" Text="Oddelek:"></asp:Label>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DropDownList ID="DDL_Department" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_Department_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
        <hr />
        <div>
            <asp:GridView ID="GV_Employee" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSource1" Width="703px" BorderStyle="None" DataKeyNames="EmployeeID" GridLines="Horizontal" OnSelectedIndexChanged="GV_Employee_SelectedIndexChanged" PageSize="5" OnPageIndexChanged="GV_Employee_PageIndexChanged">
                <Columns>
                   <asp:TemplateField HeaderText="Ime in priimek" SortExpression="LastName">
                        <EditItemTemplate>
                             <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("FirstName") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label1" runat="server" Text='<%# Bind("FirstName") %>'></asp:Label>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("LastName") %>'></asp:Label>
                         </ItemTemplate>
                        <ItemStyle Font-Bold="False" />
                    </asp:TemplateField>
                    <asp:BoundField DataField="JobTitle" HeaderText="Delovno mesto" SortExpression="State" >
                    <ControlStyle Font-Bold="True" />
                    <ItemStyle Font-Bold="False" />
                    </asp:BoundField>
                    <asp:BoundField DataField="HireDate" HeaderText="Datum zaposlitve" SortExpression="JobTitle" dataformatstring="{0:dd.MM.yyyy}" htmlencode="false" />
                    <asp:CommandField SelectText="Podrobnosti" ShowSelectButton="True" />
                </Columns>
                <HeaderStyle BackColor="Black" ForeColor="White" />
                <PagerSettings Mode="NextPrevious" />
                <PagerStyle BackColor="Black" ForeColor="White" />
                <SelectedRowStyle BorderColor="#666666" BackColor="#CCCCCC" />
            </asp:GridView>
            <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" SelectMethod="GetEmployeeListForDepartment" TypeName="_631020207_Seminar2.ServiceReference1.HRMServiceClient">
                <SelectParameters>
                    <asp:ControlParameter ControlID="DDL_Department" DefaultValue="" Name="departmentID" PropertyName="SelectedValue" Type="Int32" />
                </SelectParameters>
            </asp:ObjectDataSource>
            <br />
            <asp:DetailsView ID="DV_EmployeeDetails" runat="server" Height="50px" Width="702px" AutoGenerateRows="False" BorderStyle="None" FooterText="a" GridLines="None" HeaderText="a">
                <FieldHeaderStyle BackColor="Silver" Font-Bold="True" />
                <Fields>
                    <asp:BoundField DataField="name" HeaderText="Ime in priimek: " />
                    <asp:BoundField DataField="Gender" HeaderText="Spol: " />
                    <asp:BoundField DataField="BirthDate" HeaderText="Datum rojstva: " dataformatstring="{0:dd.MM.yyyy}" htmlencode="false" />
                    <asp:BoundField />
                    <asp:BoundField DataField="addres1" HeaderText="Naslov: " />
                    <asp:BoundField DataField="addres2" />
                    <asp:BoundField DataField="addres3" />
                    <asp:BoundField DataField="addres4" />
                    <asp:BoundField />
                    <asp:BoundField DataField="PhoneNumber" HeaderText="Telefonska številka: " />
                    <asp:BoundField DataField="EmailAddress" HeaderText="Elektronski naslov: " />
                    <asp:BoundField />
                    <asp:BoundField DataField="HireDate" HeaderText="Datum zaposlitve: " dataformatstring="{0:dd.MM.yyyy}" htmlencode="false" />
                    <asp:BoundField DataField="employmentTime" HeaderText="Delovna doba: " />
                    <asp:BoundField DataField="JobTitle" HeaderText="Delovno mesto: " />
                    <asp:BoundField DataField="shift" HeaderText="Izmena: " />
                    <asp:BoundField />
                    <asp:BoundField DataField="pay" HeaderText="Urna postavka: " />
                </Fields>
                <FooterStyle BackColor="Black" />
                <HeaderStyle BackColor="Black" />
            </asp:DetailsView>
            <hr />
            <div>
                <asp:Button ID="B_Orders" runat="server" Font-Bold="True" OnClick="B_Orders_Click" Text="Prikaži ustvarjen promet" Height="26px" Visible="False" />
                <br />
                <br />
                <asp:Label ID="L_SaleStatistics1p" runat="server" Font-Bold="True" Text="Število strank:" Visible="False"></asp:Label>
&nbsp;<asp:Label ID="L_SaleStatistics1" runat="server" Font-Bold="True" ForeColor="Black" Text="Label" Visible="False"></asp:Label>
                <br />
                <asp:Label ID="L_SaleStatistics2p" runat="server" Font-Bold="True" Text="Število naročil:" Visible="False"></asp:Label>
&nbsp;<asp:Label ID="L_SaleStatistics2" runat="server" Font-Bold="True" Text="Label" Visible="False"></asp:Label>
                <br />
                <asp:Label ID="L_SaleStatistics3p" runat="server" Font-Bold="True" Text="Skupna vrednost naročil:" Visible="False"></asp:Label>
&nbsp;<asp:Label ID="L_SaleStatistics3" runat="server" Text="Label" Visible="False"></asp:Label>
&nbsp;<br />
                <br />
                <asp:GridView ID="GV_Orders" runat="server" AllowPaging="True" AutoGenerateColumns="False" DataSourceID="ObjectDataSource2" GridLines="None" Width="707px" Visible="False" PageSize="20">
                    <Columns>
                        <asp:BoundField DataField="OrderNumber" HeaderText="Naročilo" SortExpression="OrderNumber" />
                        <asp:BoundField DataField="OrderDate" HeaderText="Datum" SortExpression="OrderDate" dataformatstring="{0:dd.MM.yyyy}" htmlencode="false" />
                        <asp:BoundField DataField="Customer" HeaderText="Stranka" SortExpression="Customer" />
                        <asp:BoundField DataField="Amount" HeaderText="Število izdelkov" SortExpression="Amount" HeaderStyle-Width="10%" ItemStyle-Width="10%" FooterStyle-Width="10%" />
                        <asp:BoundField DataField="SubTotal" HeaderText="Vrednost" SortExpression="SubTotal" DataFormatString="{0:C}" />
                        <asp:BoundField DataField="TaxAmt" HeaderText="Davek" SortExpression="TaxAmt" DataFormatString="{0:C}" />
                        <asp:BoundField DataField="Freight" HeaderText="Dostava" SortExpression="Freight" DataFormatString="{0:C}" />
                        <asp:BoundField DataField="TotalDue" HeaderText="Za plačilo" SortExpression="TotalDue" DataFormatString="{0:C}" />
                    </Columns>
                    <HeaderStyle BackColor="Black" ForeColor="White" />
                    <PagerSettings Mode="NextPrevious" PageButtonCount="20" />
                    <PagerStyle BackColor="Black" ForeColor="White" />
                </asp:GridView>
                <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" SelectMethod="GetOrderList" TypeName="_631020207_Seminar2.ServiceReference1.HRMServiceClient">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="GV_Employee" Name="employeeID" PropertyName="SelectedValue" Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </div>
        </div>
    </form>
</body>
</html>
