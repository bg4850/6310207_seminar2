﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _631020207_Seminar2
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var websr = new ServiceReference1.HRMServiceClient();
            if (!IsPostBack)
            {
                var ddlDepGroup = (from x in websr.GetDepartmentList()
                                   select  x.DepartmentGroupName).Distinct();
                DDL_DepartmentGroup.DataSource = ddlDepGroup;
                DDL_DepartmentGroup.DataBind();

                var ddlDep = (from x in websr.GetDepartmentList()
                              where DDL_DepartmentGroup.SelectedValue.ToString() == x.DepartmentGroupName.ToString()
                              select new
                              {
                                  x.DepartmentName,
                                  x.DepartmentID
                              }).Distinct();
                foreach (var x in ddlDep)
                {
                    DDL_Department.Items.Add(new ListItem(x.DepartmentName, Convert.ToString(x.DepartmentID)));
                }
            }

            
        }

        protected void hideDiv4()
        {
            L_SaleStatistics1.Visible = false;
            L_SaleStatistics2.Visible = false;
            L_SaleStatistics3.Visible = false;
            L_SaleStatistics1p.Visible = false;
            L_SaleStatistics2p.Visible = false;
            L_SaleStatistics3p.Visible = false;
            GV_Orders.Visible = false;
        }

        protected void DDL_DeparmentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Employee.SelectedIndex = -1;
            GV_Employee.PageIndex = 0;
            DV_EmployeeDetails.Visible = false;
            B_Orders.Visible = false;
            hideDiv4();
            var websr = new ServiceReference1.HRMServiceClient();
            var ddlDep = (from x in websr.GetDepartmentList()
                          where DDL_DepartmentGroup.SelectedValue.ToString() == x.DepartmentGroupName.ToString()
                          select new
                          {
                              x.DepartmentName,
                              x.DepartmentID
                          }).Distinct();
            DDL_Department.Items.Clear();
            foreach (var x in ddlDep) 
            { 
                DDL_Department.Items.Add(new ListItem(x.DepartmentName, Convert.ToString(x.DepartmentID))); 
            }
            DDL_Department_SelectedIndexChanged(null, null);
        }

        protected void DDL_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            GV_Employee.SelectedIndex = -1;
            GV_Employee.PageIndex = 0;
            DV_EmployeeDetails.Visible = false;
            B_Orders.Visible = false;
            hideDiv4();
        }

        protected void GV_Employee_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DDL_Department.SelectedItem.Text.Equals("Sales"))
                B_Orders.Visible = true;

            hideDiv4();
            DV_EmployeeDetails.Visible = true;
            var websr = new ServiceReference1.HRMServiceClient();
            var dvemp = from x in websr.GetEmployeeList()
                        where GV_Employee.SelectedValue.ToString() == x.EmployeeID.ToString()
                        select new
                        {
                            name = x.FirstName + " " + x.LastName,
                            x.Gender,
                            x.BirthDate,
                            addres1 = x.Address,
                            addres2 = x.City + " " + x.PostalCode,
                            addres3 = x.State,
                            addres4 = x.Country,
                            x.PhoneNumber,
                            x.EmailAddress,
                            x.HireDate,
                            employmentTime = (DateTime.Now.Year - x.HireDate.Year - 1) + " let",
                            x.JobTitle,
                            shift = x.Shift + " ( " + x.ShiftStartTime + " - " + x.ShiftEndTime + " )",
                            pay = x.PayRate.ToString("C2")
                        };
            DV_EmployeeDetails.DataSource = dvemp.ToList();
            DV_EmployeeDetails.DataBind();
        }

        protected void B_Orders_Click(object sender, EventArgs e)
        {
            GV_Orders.DataBind();
            var websr = new ServiceReference1.HRMServiceClient();
            L_SaleStatistics1.Visible = true;
            L_SaleStatistics2.Visible = true;
            L_SaleStatistics3.Visible = true;
            L_SaleStatistics1p.Visible = true;
            L_SaleStatistics2p.Visible = true;
            L_SaleStatistics3p.Visible = true;
            GV_Orders.Visible = true;
            
            var s1 = from x in websr.GetOrderList(Convert.ToInt32(GV_Employee.SelectedValue.ToString()))
                               select x;
            var s2 = (from x in websr.GetOrderList(Convert.ToInt32(GV_Employee.SelectedValue.ToString()))
                      select x.Customer).Distinct();
            var s3 = (from x in websr.GetOrderList(Convert.ToInt32(GV_Employee.SelectedValue.ToString()))
                     select x.SubTotal).Sum();
            L_SaleStatistics1.Text = s2.Count().ToString();
            L_SaleStatistics2.Text = s1.Count().ToString();
            L_SaleStatistics3.Text = s3.ToString("C2");
        }

        protected void GV_Employee_PageIndexChanged(object sender, EventArgs e)
        {
            GV_Employee.SelectedIndex = -1;
        } 

    }
}